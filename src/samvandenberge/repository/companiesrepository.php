<?php

namespace Samvandenberge\Repository;

class CompaniesRepository extends \Knp\Repository {

    public function getTableName() {
        return 'companies';
    }


    /*------------------
     * Select
     * -----------------
    */

    // get a specific company by it's email address
    public function getCompany($email) {
        return $this->db->fetchAssoc('SELECT * from companies WHERE companies.email = ?', array($email));
    }

    // get only the names from companies that offer internships
   public function getCompanyNames() {
        return $this->db->fetchAll('SELECT companies.name FROM companies
            INNER JOIN internships on internships.companies_id = companies.id
           GROUP by companies.name ORDER BY LOWER(companies.name)
        ');
    }

    // get a company's details by it's id
    public function getCompanyInfoById($id) {
        return $this->db->fetchAssoc(
            'SELECT companies.name as name, companies.logo, companies.password, companies.logo, companies.tel, companies.fax, companies.email, companies.website,
            addresses.id as address_id, addresses.address, addresses.city, countries.name as countryname, countries.id as country_id, addresses.postal_code
            FROM companies
            INNER JOIN addresses ON addresses.id = companies.addresses_id
            INNER JOIN countries on countries.id = addresses.countries_id
            WHERE companies.id = ?
            ORDER BY countries.id;', array($id));
    }

    // get a company's details by an internship id
    public function getCompanyInfoByInternship($id) {
        return $this->db->fetchAssoc('
        SELECT companies.name FROM internships
        INNER JOIN companies on companies.id = internships.companies_id
        WHERE internships.companies_id = ?
        ', array($id));
    }

    // get a list of all country names
    public function getCountries() {
        return $this->db->fetchAll('SELECT countries.name from countries ORDER BY countries.name ASC');
    }


    /*------------------
    * Insert
    * -----------------
   */

    // add a new company
    public function addCompany($data, $app) {
        $this->db->insert('addresses', array(
            'address' => $data['Address'],
            'city' => $data['City'],
            'postal_code' => $data['Postalcode'],
            'countries_id' => (int)$data['Country'] + 1 // values start from 0 instead of 1 in the db
        ));

        $this->db->insert('companies', array(
            'addresses_id' => $this->db->lastInsertId(),
            'name' => $data['Name'],
            'password' => crypt($data['Password'], $app['encryption']['SALT']),
            'tel' => $data['Tel'],
            'fax' => $data['Fax'],
            'email' => $data['Email'],
            'website' => $data['Website']
        ));
    }


    /*------------------
    * Update
    * -----------------
   */

    // update a company
    public function updateCompany($data, $user, $app) {
        $id = $app['session']->get('user')['id'];

        $this->db->update('addresses', array(
                'address' => $data['Address'],
                'city' => $data['City'],
                'postal_code' => $data['Postalcode'],
                'countries_id' => (int)$data['Country'] + 1 // values start from 0 instead of 1 in the db
            ), array('id' => $user['address_id'])
        );

        $this->db->update('companies', array(
                'addresses_id' => $user['address_id'],
                'name' => $data['Name'],
                'password' => $data['Password'],
                'tel' => $data['Tel'],
                'fax' => $data['Fax'],
                'email' => strtolower($data['Email']),
                'website' => $data['Website'],
                'logo' => $data['Logo']
            ), array('id' => $id)
        );
    }
}