<?php

namespace Samvandenberge\Repository;

class SubcategoriesRepository extends \Knp\Repository {

    public function getTableName() {
        return 'subcategories';
    }


    /*------------------
     * Select
     * -----------------
    */

    // get all subcategories by - alphabetically ordered by name
    public function getSubcategories() {
        return $this->db->fetchAll('SELECT * from subcategories ORDER BY name');
    }

    // get a specific subcategorie by it's id
    public function getSubcategoryName($id) {
        return $this->db->fetchAssoc('SELECT subcategories.name from subcategories WHERE subcategories.id = ?', array($id));
    }

    // get a subcategorie id by giving it's name
    public function getCategoryId($name) {
        return $this->db->fetchAssoc('SELECT subcategories.categories_id from subcategories WHERE subcategories.name =  ?', array($name));
    }
}