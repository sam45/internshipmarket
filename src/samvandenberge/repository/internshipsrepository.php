<?php

namespace Samvandenberge\Repository;

class InternshipsRepository extends \Knp\Repository {

    public function getTableName() {
        return 'internships';
    }


    /*------------------
     * Select
     * -----------------
    */

    // get all internships on company id
    public function getInternships($userid) {
        return $this->db->fetchAll('SELECT * from internships WHERE internships.companies_id = ?', array($userid));
    }

    // get a specific internship from a specific company
    public function getInternship($userid, $id) {
        return $this->db->fetchAll('SELECT * from internships WHERE internships.companies_id = ? and internships.id = ?', array($userid, $id));
    }

    // select everything with a start date later than today for the internships overview
    // and sort on start date (closest first)
    public function getAllInternships() {
        return $this->db->fetchAll('
            SELECT
            companies.name as companyname, companies.logo as logo, internships.name as title, internships.id, countries.name as country, categories.name as category, internships.date_start, internships.date_end
            FROM internships
            INNER JOIN companies ON companies.id = internships.companies_id
            INNER JOIN addresses ON addresses.id = companies.addresses_id
            INNER JOIN countries ON countries.id = addresses.countries_id
            INNER JOIN subcategories ON subcategories.id = internships.subcategories_id
            INNER JOIN categories ON categories.id = subcategories.categories_id
            HAVING internships.date_start >= CURDATE()
            ORDER BY ABS(DATEDIFF(internships.date_start, NOW() )) ASC;
        ');
    }

    // get a specific internship by it's id
    public function getInternshipById($id) {
        return $this->db->fetchAssoc('
            SELECT internships.id, internships.name, internships.free_places, internships.description, internships.date_start, internships.date_end, subcategories.name as subcategory, companies_id, internships.companies_id
            FROM internships
            INNER JOIN subcategories ON subcategories.id = internships.subcategories_id
            WHERE internships.id = ?', array($id));
    }

    // get the related internships by same subcategory
    // sort on name
    public function getRelatedInternships($subcategory) {
        return $this->db->fetchAll('
          SELECT internships.name, internships.id FROM internships
          INNER JOIN subcategories ON subcategories.id = internships.subcategories_id
          WHERE subcategories.name = ?
          ORDER BY internships.name', array($subcategory));
    }


    /*------------------
     * Select : search/filter
     * -----------------
    */

    // search internships on internship name
    // show only those with a later start date than today and sort on date
    public function getSearchedInternships($searchstring) {
        return $this->db->fetchAll('
            SELECT
            companies.name as companyname, companies.logo as logo, internships.name as title, internships.id, countries.name as country, categories.name as category, internships.date_start, internships.date_end
            FROM internships
            INNER JOIN companies ON companies.id = internships.companies_id
            INNER JOIN addresses ON addresses.id = companies.addresses_id
            INNER JOIN countries ON countries.id = addresses.countries_id
            INNER JOIN subcategories ON subcategories.id = internships.subcategories_id
            INNER JOIN categories ON categories.id = subcategories.categories_id
            WHERE internships.name like ?
            HAVING internships.date_start >= CURDATE()
            ORDER BY ABS(DATEDIFF(internships.date_start, NOW() )) ASC', array("%{$searchstring}%"));
    }

    // filter internships on several parameters
    // show only those with a later start date than today and sort on date
    public function getFilteredInternships($data) {
        return $this->db->fetchAll('
            SELECT
            companies.name as companyname, companies.logo as logo, internships.name as title, internships.id, countries.name as country, categories.name as category,
            internships.date_start, internships.date_end
            FROM internships
            INNER JOIN companies ON companies.id = internships.companies_id
            INNER JOIN addresses ON addresses.id = companies.addresses_id
            INNER JOIN countries ON countries.id = addresses.countries_id
            INNER JOIN subcategories ON subcategories.id = internships.subcategories_id
            INNER JOIN categories ON categories.id = subcategories.categories_id
            WHERE countries.id like ?
            AND companies.name like ?
            AND categories.name like ?
            AND internships.date_start > ?
            AND internships.date_end < ?
            HAVING internships.date_start >= CURDATE()
            ORDER BY ABS(DATEDIFF(internships.date_start, NOW() )) ASC
            ', array("{$data['Country']}", "{$data['Company']}", "{$data['Category']}", $data['Start'], $data['End']));
    }

    // get all internships from a specific company
    // show only those with a later start date than today and sort on date
    public function getAllInternshipsFromCompany($company) {
        return $this->db->fetchAll('
            SELECT
            companies.name as companyname, companies.logo as logo, internships.name as title, internships.id, countries.name as country, categories.name as category, internships.date_start, internships.date_end
            FROM internships
            INNER JOIN companies ON companies.id = internships.companies_id
            INNER JOIN addresses ON addresses.id = companies.addresses_id
            INNER JOIN countries ON countries.id = addresses.countries_id
            INNER JOIN subcategories ON subcategories.id = internships.subcategories_id
            INNER JOIN categories ON categories.id = subcategories.categories_id
            WHERE companies.name = ?
            HAVING internships.date_start >= CURDATE()
            ORDER BY ABS(DATEDIFF(internships.date_start, NOW() )) ASC
            ', array($company));
    }


    /*------------------
     * Delete
     * -----------------
    */

    // delete an internship by id
    public function deleteInternship($id, $comp_id) {
        $this->db->delete('internships', array(
            'id' => $id,
            'companies_id' => $comp_id
        ));
    }

    /*------------------
     * Update
     * -----------------
    */

    // update an internship
    public function updateInternship($data, $userId, $internshipId) {
        $this->db->update('internships', array(
                'companies_id' => $userId,
                'subcategories_id' => $data['Subcategory'],
                'name' => $data['Title'],
                'free_places' => $data['Free'],
                'description' => $data['Description'],
                'date_start' => $data['Start'],
                'date_end' => $data['End']
            ),
            array('id' => $internshipId));
    }

    /*------------------
     * Insert
     * -----------------
    */

    // add a new internship
    public function addInternship($data, $id) {
        $this->db->insert('internships', array(
            'companies_id' => $id,
            'subcategories_id' => $data['Subcategory'],
            'name' => $data['Title'],
            'free_places' => $data['Free'],
            'description' => $data['Description'],
            'date_start' => $data['Start'],
            'date_end' => $data['End']
        ));
    }
}