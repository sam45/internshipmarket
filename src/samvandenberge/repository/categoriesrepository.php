<?php

namespace Samvandenberge\Repository;

class CategoriesRepository extends \Knp\Repository {

    public function getTableName() {
        return 'categories';
    }


    /*------------------
     * Select
     * -----------------
    */

    // get all categories
    public function getCategories() {
        return $this->db->fetchAll('SELECT * from categories ORDER BY categories.name');
    }

    // get a specific category by it's id
    public function getCategory($id) {
        return $this->db->fetchAssoc('SELECT categories.name from categories where categories.id = ?', array($id));
    }
}