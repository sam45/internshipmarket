<?php

namespace Samvandenberge\Provider\Controller;

use Pagerfanta\Pagerfanta;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Pagerfanta\Adapter\ArrayAdapter;

class FrontController implements ControllerProviderInterface {

    /**
     * Create the controllers
     * @param Application $app
     * @return ControllerCollection
     */
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers
            ->get('/', array($this, 'internships'))
            ->method('GET|POST')
            ->bind('front.internships');
        $controllers
            ->get('/{id}', array($this, 'detail'))
            ->assert('id', '\d+')
            ->bind('front.detail');
        $controllers
            ->get('/{id}/contact', array($this, 'contact'))
            ->assert('id', '\d+')
            ->method('GET|POST')
            ->bind('front.contact');

        return $controllers;
    }

    /**
     * Get all internships and set up filters
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function internships(Application $app) {
        // var to see if the results were filtered or not
        $isFiltered = false;

        // get a list of the countries to populate the dropdown
        $countries = $app['users']->getCountries();
        $countryNames = array();
        foreach ($countries as $country) {
            $countryNames[] = $country['name'];
        }
        // get the subcategories from the database
        $categories = $app['categories']->getCategories();
        $categoryNames = array();
        foreach ($categories as $category) {
            $categoryNames[] = $category['name'];
        }
        // get the companies from the database
        $companies = $app['users']->getCompanyNames();
        $companieNames = array();
        foreach ($companies as $company) {
            $companieNames[] = $company['name'];
        }

        // Create Search form
        $internshipsearch = $app['form.factory']->createNamed('internshipsearch')
            ->add('Search', 'text', array(
                'required' => false,
                'label' => 'Search for an internship'
            ));

        // Create Filter Form
        $internshipfilter = $app['form.factory']->createNamed('internshipfilter')
            ->add('Company', 'choice', array(
                'choices' => $companieNames,
                'empty_value' => 'Choose an option',
                'required' => false
            ))
            ->add('Country', 'choice', array(
                'choices' => $countryNames,
                'empty_value' => 'Choose an option',
                'required' => false
            ))
            ->add('Category', 'choice', array(
                'choices' => $categoryNames,
                'empty_value' => 'Choose an option',
                'required' => false
            ))
            // date doesn't work without the php 'intl' extension
            ->add('Start', 'text', array(
                'attr' => array('class' => 'date', 'placeholder' => 'dd-mmm-yyyy'),
                'label' => 'From',
                'required' => false
            ))
            // date doesn't work without the php 'intl' extension
            ->add('End', 'text', array(
                'attr' => array('class' => 'date', 'placeholder' => 'dd-mmm-yyyy'),
                'label' => 'To',
                'required' => false
            ));

        // Determine which form was submitted
        $type = explode('&', ($app['request']->getContent()));
        $type = $type[sizeof($type) - 1];

        // Search Form was submitted: process it
        if ('POST' == $app['request']->getMethod() && $type == 'submit=Search') {
            $internshipsearch->bind($app['request']);
            $data = $internshipsearch->getData();
            // get results
            $internships = $app['internships']->getSearchedInternships($data['Search']);
            $isFiltered = true;
        }

        // Filter Form was submitted: process it
        if ('POST' == $app['request']->getMethod() && $type == 'submit=Filter') {
            $internshipfilter->bind($app['request']);
            $data = $internshipfilter->getData();

            // data in the country table is alphabetically, we only have to increase by one (dropdown starts from 0, id from 1)
            $data['Country'] = ($data['Country'] === null) ? '%' : $data['Country'] + 1;
            // data in the categories table is NOT alphabetically, we have to find the corresponding name first
            $data['Category'] = ($data['Category'] === null) ? '%' : $data['Category'];
            // data in the companies table is NOT alphabetically, we have to find the corresponding name first
            $data['Company'] = ($data['Company'] === null) ? '%' : $data['Company'];

            // find corresponding names
            if ($data['Category'] !== '%') {
                for ($i = 0; $i < sizeof($categories); $i++) {
                    if ($i === (int)$data['Category']) {
                        $data['Category'] = $categories[$i]['name'];
                    }
                }
            }
            if ($data['Company'] !== '%') {
                for ($i = 0; $i < sizeof($companies); $i++) {
                    if ($i === (int)$data['Company']) {
                        $data['Company'] = $companies[$i]['name'];
                    }
                }
            }

            // convert European date format to American to compare it and store it in the database
            $data['Start'] = ($data['Start'] === null) ? '0' : date("Y-m-d", strtotime($data['Start']));
            $data['End'] = ($data['End'] === null) ? PHP_INT_MAX : date("Y-m-d", strtotime($data['End']));

            // get the filtered data and set boolean
            $internships = $app['internships']->getFilteredInternships($data);
            $isFiltered = true;
        }

        // check if the company GET parameter was set
        if (isset($_GET['company'])) {
            $company = $_GET['company'];
            // get all internships from a specific company and set the boolean
            $internships = $app['internships']->getAllInternshipsFromCompany($company);
            $isFiltered = true;
        }

        // if there isn't any filter applied or there isn't a search, get all internships
        if (!isset($internships)) {
            $internships = $app['internships']->getAllInternships();
        }

        // calculate the duration of all internships
        for ($i = 0; $i < sizeof($internships); $i++) {
            $duration = strtotime($internships[$i]['date_end'], 0) - strtotime($internships[$i]['date_start'], 0);
            $duration = (int)round($duration / 604800);
            $internships[$i]['duration'] = $duration;
        }

        // pagination
        //@see http://frenchtouch.pro/tutorial/install-and-manage-pagerfanta-in-symfony2/20
        $adapter = new ArrayAdapter($internships);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);

        // if $page doesn't exist, fix it to page 1
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $pagerfanta->setCurrentPage($page);

        return $app['twig']->render('frontend/internships.twig', array('internshipsearch' => $internshipsearch->createView(), 'internshipfilter' => $internshipfilter->createView(), 'filtered' => $isFiltered, 'countries' => $countries, 'internships' => $internships, 'pagerfanta' => $pagerfanta));
    }

    /**
     * Show the details of an internship
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function detail(Application $app, $id) {
        // get the internship
        $internship = $app['internships']->getInternshipById($id);
        // parameter tempering
        if (!$internship) {
            $app->abort(404, 'Internship ' . $id . ' does not exist');
        }

        // get the data from the repository
        $company = $app['users']->getCompanyInfoByInternship($internship['companies_id']);
        $relatedInternships = $app['internships']->getRelatedInternships($internship['subcategory']);

        return $app['twig']->render('frontend/detail.twig', array('internship' => $internship, 'relatedInternships' => $relatedInternships, 'company' => $company));
    }

    /**
     * Contact a company for an internship
     * @param Application $app
     * @param $id
     * @see http://thecancerus.com/how-to-get-latitudelongitude-from-an-address-or-geocoding-using-php/
     * @return mixed
     */
    public function contact(Application $app, $id) {
        // get the internship
        $internship = $app['internships']->getInternshipById($id);
        // parameter tempering
        if (!$internship) {
            $app->abort(404, 'Internship ' . $id . ' does not exist');
        }

        // get the company
        $company = $app['users']->getCompanyInfoById($internship['companies_id']);

        // fix encoding of special characters in some foreign countries address
        $company['address'] = utf8_encode($company['address']);
        $company['city'] = utf8_encode($company['city']);

        // get the address
        $address = $company['address'] . ', ' . $company['countryname'];

        // replace spaces by '+', needed for the Google API to work
        $address = str_replace(' ', '+', $address);

        // get the longtitude and latitude
        $url = 'http://maps.google.com/maps/api/geocode/json?address=' . $address . '&output=json&oe=utf8&sensor=false';
        $geocode = json_decode(@file_get_contents($url));

        // save coordinates in an associative array
        $coordinates = array();
        $coordinates['lat'] = $geocode->results[0]->geometry->location->lat;
        $coordinates['long'] = $geocode->results[0]->geometry->location->lng;

        // motivation form
        $motivationform = $app['form.factory']->createNamed('motivationform')
            ->add('Name', 'text', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank())
            ))
            ->add('Email', 'text', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank(), new Assert\Email())
            ))
            ->add('CV', 'file', array(
                'constraints' => array(new Assert\NotBlank()),
                'invalid_message' => 'Please upload your CV'
            ))
            // date doesn't work without the php 'intl' extension
            ->add('Motivation', 'textarea', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank())
            ));

        $send = isset($_GET['0']) ? 'Message sent successfully' : ''; // used for the successful/unsucessful message

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $motivationform->bind($app['request']);
            $data = $motivationform->getData();

            // check if the entered data is valid
            if ($motivationform->isValid()) {
                $data = $motivationform->getData();

                // upload CV and determine type
                if (isset($data['CV'])) {
                    $filetype = null;
                    if (substr($data['CV']->getClientOriginalName(), -5) === '.docx') {
                        $filetype = '.docx';
                    } else if (substr($data['CV']->getClientOriginalName(), -4) === '.doc') {
                        $filetype = '.doc';
                    } else if (substr($data['CV']->getClientOriginalName(), -4) === '.odp') {
                        $filetype = '.odp';
                    } else if (substr($data['CV']->getClientOriginalName(), -4) === '.pdf') {
                        $filetype = '.pdf';
                    }

                    // when supported
                    if ($filetype !== null) {
                        // Gmail doesn' allow overriding the FROM name except from verfied email addresses that you prove to gmail you own.
                        // Set the reply email as the users' email
                        $message = \Swift_Message::newInstance()
                            // for testing purposes: use own email instead of the company's email (the email addresses in the database are real)
                            ->setTo(array('van.den.berge.sam@gmail.com'))
                            ->setSubject('Application for ' . $internship['name'] . ' - ' . $data['Name'])
                            ->setSender(array($data['Email']))
                            ->setReplyTo(array($data['Email']))
                            ->setBody($data['Motivation'])
                            // Attach the uploaded file to the message and give the attachment a useful name
                            ->attach(\Swift_Attachment::newInstance(file_get_contents($data['CV']), 'CV - ' . $data['Name'] . $filetype, null));

                        // send email and determine if it was scucesful or not
                        if ($app['mailer']->send($message)) {
                            $send = 'Message sent successfully.';
                            return $app->redirect($app['url_generator']->generate('front.contact', array('id' => $id, 'sent')));

                        } else {
                            $send = 'Something went wrong, please try again.';
                        }
                    } else {
                        $motivationform->get('CV')->addError(new \Symfony\Component\Form\FormError('Only .docx, .pdf, .doc or .odp is supported'));
                    }
                }
            }
        }
        return $app['twig']->render('frontend/contact.twig', array('coordinates' => $coordinates, 'internship' => $internship, 'company' => $company, 'motivationform' => $motivationform->createView(), 'send' => $send));
    }
}