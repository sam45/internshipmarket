<?php

namespace Samvandenberge\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;

class AuthController implements ControllerProviderInterface {
    /**
     * @param Application $app
     * @return ControllerCollection
     */
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', function (Application $app) {
            return $app->redirect($app['url_generator']->generate('auth.login'));
        });

        $controllers
            ->match('/login', array($this, 'login'))
            ->method('GET|POST')
            ->bind('auth.login');
        $controllers
            ->get('/logout', array($this, 'logout'))
            ->assert('id', '\d+')
            ->bind('auth.logout');
        $controllers
            ->get('/register', array($this, 'register'))
            ->method('GET|POST')
            ->bind('auth.register');
        $controllers
            ->get('/edit', array($this, 'edit'))
            ->bind('auth.edit');

        return $controllers;
    }

    /**
     * Login CMS
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function login(Application $app) {
        // Already logged in
        if ($app['session']->get('user')) {
            return $app->redirect($app['url_generator']->generate('management.overview'));
        }

        // Create Form
        $loginform = $app['form.factory']->createNamed('loginform')
            ->add('Email', 'email', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Email()),
                'invalid_message' => 'You entered an invalid value',
            ))
            ->add('Password', 'password', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $loginform->bind($app['request']);

            // check if the entered data is valid
            if ($loginform->isValid()) {
                $data = $loginform->getData();
                $data['Email'] = strtolower($data['Email']);

                // get the user from the database
                $user = $app['users']->getCompany($data['Email']);
                // check if the account exists
                if (!$user) {
                    $loginform->get('Email')->addError(new \Symfony\Component\Form\FormError('The email address you entered is incorrect.'));
                }

                // check if the password is correct
                if (crypt($data['Password'], $app['encryption']['SALT']) === $user['password']) {
                    // create session
                    $app['session']->set('user', array(
                        'id' => $user['id'],
                        'username' => $data['Email'],
                        'company' => $user['name']
                    ));

                    // go to the overview
                    return $app->redirect($app['url_generator']->generate('management.overview'));
                } else {
                    $loginform->get('Password')->addError(new \Symfony\Component\Form\FormError('The password you entered is incorrect.'));
                }
            }
        }

        return $app['twig']->render('auth/login.twig', array('loginform' => $loginform->createView()));
    }

    /**
     * Register
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function register(Application $app) {
        // Already logged in - logout first before registering
        if ($app['session']->get('user')) {
            $app['session']->remove('user');
            return $app->redirect($app['url_generator']->generate('auth.logout') . '?loggedout');
        }

        // get a list of the countries to populate the dropdown
        $countries = $app['users']->getCountries();
        // convert the 2D array into a 1D array with only the names
        $countryNames = array();
        foreach ($countries as $country) {
            $countryNames[] = $country['name'];
        }

        // Create Form
        $registerform = $app['form.factory']->createNamed('registerform')
            ->add('Name', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('Password', 'password', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('Password2', 'password', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('Email', 'email', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Email())
            ))
            ->add('Email2', 'email', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Email())
            ))
            ->add('Tel', 'text', array(
                'constraints' => array(new Assert\Length(array('min' => 11)))
            ))
            ->add('Fax', 'text', array(
                'constraints' => array(new Assert\Length(array('min' => 11)))
            ))
            ->add('Website', 'url', array(
                'default_protocol' => 'http',
                'constraints' => array(new Assert\Url())
            ))
            ->add('Address', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('City', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            // number doesn't work without installing the 'intl extension' @TODO test on server before implementing
            ->add('Postalcode', 'text', array(
                'label' => 'Postal Code',
                'constraints' => array(new Assert\Length(array('min' => 4)))
            ))
            ->add('Country', 'choice', array(
                'choices' => $countryNames,
                'empty_value' => 'Choose an option',
                'constraints' => array(new Assert\NotBlank()
                )));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $registerform->bind($app['request']);
            $data = $registerform->getData();
            $data['Email'] = strtolower($data['Email']);


            // get the user from the database
            $user = $app['users']->getCompany($data['Email']);

            // check if the entered data is valid
            if ($registerform->isValid()) {
                $isValid = true;

                // check if the email address is twice the same
                if ($data['Email'] !== strtolower($data['Email2'])) {
                    $registerform->get('Email2')->addError(new \Symfony\Component\Form\FormError('Please verify the email address.'));
                    $isValid = false;
                }

                // check if the password is twice the same
                if ($data['Password'] !== $data['Password2']) {
                    $registerform->get('Password2')->addError(new \Symfony\Component\Form\FormError('Please verify the password.'));
                    $isValid = false;
                }

                // check if the email address is already in the database
                if ($user > 0) {
                    $registerform->get('Email')->addError(new \Symfony\Component\Form\FormError('The email address you entered already exists.'));
                    $isValid = false;
                }

                // write to database if everything is ok
                if ($isValid) {
                    $app['users']->addCompany($data, $app);

                    // registration succesfull
                    return $app->redirect($app['url_generator']->generate('auth.login'));
                }
            }
        }

        return $app['twig']->render('auth/register.twig', array('registerform' => $registerform->createView(), 'countries' => $countries));
    }

    /**
     * Logout from the CMS
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(Application $app) {
        $app['session']->remove('user');

        return $app->redirect($app['url_generator']->generate('auth.login') . '?loggedout');
    }
}