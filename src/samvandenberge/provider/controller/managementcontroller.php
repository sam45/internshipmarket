<?php

namespace Samvandenberge\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;

class ManagementController implements ControllerProviderInterface {
    /**
     * @param Application $app
     * @return ControllerCollection
     */
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers
            ->get('/', array($this, 'overview'))
            ->bind('management.overview')
            ->before(array($this, 'checkLogin'));
        $controllers
            ->get('/delete/{id}', array($this, 'delete'))
            ->assert('id', '\d+')
            ->before(array($this, 'checkLogin'));
        $controllers
            ->get('/edit/{id}', array($this, 'edit'))
            ->assert('id', '\d+')
             ->bind('management.edit')
              ->method('GET|POST')
            ->before(array($this, 'checkLogin'));
        $controllers
            ->get('/add', array($this, 'add'))
            ->bind('management.add')
            ->method('GET|POST')
            ->before(array($this, 'checkLogin'));

        $controllers
            ->get('/company', array($this, 'company'))
            ->bind('management.company')
            ->method('GET|POST')
            ->before(array($this, 'checkLogin'));

        return $controllers;
    }

    /**
     * Overview internships
     * @param Application $app
     * @return mixed
     */
    public function overview(Application $app) {
        // Get all internships for the current user
        $internships = $app['internships']->getInternships($app['session']->get('user')['id']);

        return $app['twig']->render('management/overview.twig', array('internships' => $internships, 'user' => $app['session']->get('user')));
    }

    /**
     * Delete internship
     * @param Application $app
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Application $app, $id) {
         // get the user id
        $userId = $app['session']->get('user')['id'];

        // parameter tempering
        $internship = $app['internships']->getInternship($userId, $id);
        if (!$internship) {
            $app->abort(404, 'Internship ' . $id . ' does not exist');
        }

        // delete the internship
        $app['internships']->deleteInternship($id, $userId);

        return $app->redirect($app['url_generator']->generate('management.overview'));
    }

    /**
     * Add a new internship
     * @param Application $app
     * @return mixed
     */
    public function add(Application $app) {
        // get the subcategories from the database
        $subcategories = $app['subcategories']->getSubcategories();

        // get all the subcategory names from the list
        $subcategoryNames = array();
        foreach ($subcategories as $subcategory) {
            $subcategoryNames[] = $subcategory['name'];
        }

        // create Form
        $newform = $app['form.factory']->createNamed('registerform')
            ->add('Title', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('Description', 'textarea', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            // number doesn't work without the php 'intl' extension
            ->add('Free', 'text', array(
                'label' => 'Free places',
            ))
            // date doesn't work without the php 'intl' extension
            ->add('Start', 'text', array(
                'attr' => array('class' => 'date', 'placeholder' => 'yy-mm-dd'),

                'label' => 'Start date',
                'constraints' => array(new Assert\NotBlank())
            ))
            // date doesn't work without the php 'intl' extension
            ->add('End', 'text', array(
                'attr' => array('class' => 'date', 'placeholder' => 'yy-mm-dd'),
                'label' => 'End date',
                'constraints' => array(new Assert\NotBlank())
            ))

            ->add('Subcategory', 'choice', array(
                'choices' => $subcategoryNames,
                'empty_value' => 'Choose an option',
                'constraints' => array(new Assert\NotBlank())
            ));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $newform->bind($app['request']);
            $data = $newform->getData();

            // check number of free spots input
            if ($data['Free'] != null) {
                if (!(int)$data['Free'] > 0) {
                    // invalid
                    $newform->get('Free')->addError(new \Symfony\Component\Form\FormError('The number of available places for this internship is invalid.'));
                }
            }

            // convert European date format to American to compare it and store it in the database
            $data['Start'] = date("Y-m-d", strtotime($data['Start']));
            $data['End'] = date("Y-m-d", strtotime($data['End']));

            // check if the second date is later than the first
            if (strtotime($data['Start']) > strtotime($data['End'])) {
                $newform->get('End')->addError(new \Symfony\Component\Form\FormError('The end date is ahead of the start date.'));
            }

            // check if the entered data is valid
            if ($newform->isValid()) {
                // get the correct id, not the logical order of the dropdown (alphabetical sorting changes the order)
                $data['Subcategory'] = (int)$subcategories[$data['Subcategory']]['id'];
               // return var_dump($data);
                // write to the database
                $app['internships']->addInternship($data, $app['session']->get('user')['id']);

                // redirect to overview
                return $app->redirect($app['url_generator']->generate('management.overview'));
            }
        }

        return $app['twig']->render('management/add.twig', array('newform' => $newform->createView(), 'user' => $app['session']->get('user')));
    }


    /**
     * Edit an internship
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function edit(Application $app, $id) {
        // get the user id
        $userId = $app['session']->get('user')['id'];
        // get the internship
        $internship = $app['internships']->getInternship($userId, $id);

        // parameter tempering
        if (!$internship) {
            $app->abort(404, 'Internship ' . $id . ' does not exist');
        }

        // get the chosen subcategory
        $subcategorySelected = $app['subcategories']->getSubcategoryName((int)$internship[0]['subcategories_id']);
        // get all subcategories
        $subcategories = $app['subcategories']->getSubcategories();

        // get the right index of the chosen subcategory
        $i = 0;
        $chosenCatId = 0;
        foreach ($subcategories as $subcategory) {
            $subcategoryNames[] = $subcategory['name'];
            if ($subcategorySelected['name'] === $subcategory['name']) {
                $chosenCatId = $i;
            }
            $i++;
        }

        // convert time to EU format
        $internship[0]['date_start'] = date("d-m-Y", strtotime($internship[0]['date_start']));
        $internship[0]['date_end'] = date("d-m-Y", strtotime($internship[0]['date_end']));

        // Create Form
        $editForm = $app['form.factory']->createNamed('registerform')
            ->add('Title', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
                'data' => $internship[0]['name']
            ))

            ->add('Description', 'textarea', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5))),
                'data' => $internship[0]['description']
            ))

            // number doesn't work without the php 'intl' extension
            ->add('Free', 'text', array(
                'label' => 'Free places',
                'data' => $internship[0]['free_places']
            ))

            // date doesn't work without the php 'intl' extension
            ->add('Start', 'text', array(
                'attr' => array('class' => 'date', 'placeholder' => 'yy-mm-dd'),
                'label' => 'Start date',
                'constraints' => array(new Assert\NotBlank()),
                'data' => $internship[0]['date_start']
            ))

            // date doesn't work without the php 'intl' extension
            ->add('End', 'text', array(
                'attr' => array('class' => 'date', 'placeholder' => 'yy-mm-dd'),
                'label' => 'End date',
                'constraints' => array(new Assert\NotBlank()),
                'data' => $internship[0]['date_end']
            ))

            ->add('Subcategory', 'choice', array(
                'choices' => $subcategoryNames,
                'data' => (int)$chosenCatId,
                'empty_value' => 'Choose an option',
                'constraints' => array(new Assert\NotBlank())
            ));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $editForm->bind($app['request']);
            $data = $editForm->getData();

            // check number of free spots input
            if ($data['Free'] != null) {
                if (!(int)$data['Free'] > 0) {
                    // invalid
                    $editForm->get('Free')->addError(new \Symfony\Component\Form\FormError('The number of available places for this internship is invalid.'));
                }
            }

            // convert European date format to American to compare it and store it in the database
            $data['Start'] = date("Y-m-d", strtotime($data['Start']));
            $data['End'] = date("Y-m-d", strtotime($data['End']));

            // check if the second date is later than the first
            if (strtotime($data['Start']) > strtotime($data['End'])) {
                $editForm->get('End')->addError(new \Symfony\Component\Form\FormError('The end date is ahead of the start date.'));
            }

            // check if the entered data is valid
            if ($editForm->isValid()) {
                $data['Subcategory'] = (int)$subcategories[$data['Subcategory']]['id'];
                // insert into database
                $app['internships']->updateInternship($data, $userId, $internship[0]['id']);

                // redirect to overview
                return $app->redirect($app['url_generator']->generate('management.overview'));
            }
        }

       // return var_dump($internship);
        return $app['twig']->render('management/edit.twig', array('editform' => $editForm->createView(), 'user' => $app['session']->get('user'), 'internship' => $internship[0]));
    }


    public function company(Application $app) {
        // get the user from the database
        $user = $app['users']->getCompanyInfoById($app['session']->get('user')['id']);

        // get the countries from the database
        $countries = $app['users']->getCountries();
        // convert the 2D array into a 1D array
        $countryNames = array();
        foreach ($countries as $country) {
            $countryNames[] = $country['name'];
        }

        // Create Form
        $companyform = $app['form.factory']->createNamed('companyform')
            ->add('Name', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
                'data' => $user['name']
            ))
            ->add('Password', 'password', array(
                'constraints' => array(new Assert\Length(array('min' => 5)))
            ))
            ->add('Password2', 'password', array(
                'constraints' => array(new Assert\Length(array('min' => 5)))
            ))
            ->add('Email', 'email', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Email()),
                'data' => $user['email']
            ))
            ->add('Email2', 'email', array(
                'constraints' => array(new Assert\Email())
            ))
            ->add('Tel', 'text', array(
                'constraints' => array(new Assert\Length(array('min' => 11))),
                'data' => $user['tel']
            ))
            ->add('Fax', 'text', array(
                'constraints' => array(new Assert\Length(array('min' => 11))),
                'data' => $user['fax']
            ))
            ->add('Website', 'url', array(
                'default_protocol' => 'http',
                'constraints' => array(new Assert\Url()),
                'data' => $user['website']
            ))
            ->add('Address', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
                'data' => $user['address']
            ))
            ->add('City', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
                'data' => $user['city']
            ))
            // number doesn't work without installing the 'intl extension' @TODO test on server before implementing
            ->add('Postalcode', 'text', array(
                'label' => 'Postal Code',
                'constraints' => array(new Assert\Length(array('min' => 4))),
                'data' => $user['postal_code']

            ))
            ->add('Country', 'choice', array(
                'choices' => $countryNames,
                // -1 because values start from 0 in choices list
                'data' => (int)$user['country_id'] - 1,
                'empty_value' => 'Choose an option',
                'constraints' => array(new Assert\NotBlank())
            ))
            ->add('Logo', 'file', array(
                 'label' => 'Logo (.jpg)',
            ));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $companyform->bind($app['request']);
            $data = $companyform->getData();
            $data['Email'] = strtolower($data['Email']);
            // check if the entered data is valid
            if ($companyform->isValid()) {
                $isValid = true;

                // if the email address is changed
                if ($data['Email'] !== $user['email']) {
                    // check if the email address is twice the same
                    if ($data['Email'] !== strtolower($data['Email2'])) {
                        $companyform->get('Email2')->addError(new \Symfony\Component\Form\FormError('Please verify the email address.'));
                        $isValid = false;
                    }
                    // check if the email address is already in the database
                    $testuser = $app['users']->getCompany($data['Email']);
                    if ($testuser > 0) {
                        $companyform->get('Email')->addError(new \Symfony\Component\Form\FormError('The email address you entered already exists.'));
                        $isValid = false;
                    }
                }

                // if the password is changed
                if (crypt($data['Password'], $app['encryption']['SALT']) !== $user['password']) {

                    // check if the password is twice the same
                    if ($data['Password'] !== $data['Password2']) {
                        $companyform->get('Password2')->addError(new \Symfony\Component\Form\FormError('Please verify the password.'));
                        $isValid = false;
                    }
                    // make sure the password isn't empty
                    if (sizeof($data['Password']) > 0) {
                        // crypt the password
                        $data['Password'] = crypt($data['Password'], $app['encryption']['SALT']);
                        // keep the old one
                        } else {
                        $data['Password'] = $user['password'];
                    }
                }

                // logo upload - must be .jpg
				if (isset($data['Logo']) && ('.jpg' === substr($data['Logo']->getClientOriginalName(), -4))) {

					// Define the new name (files are named sequentially)
					$nextAvailableNumberInBasePath = 1;
					$di = new \DirectoryIterator($app['management.base_path']);
					foreach ($di as $file) {
						if ($file->getExtension() == 'jpg') $nextAvailableNumberInBasePath++;
					}

                    // Move it to its new location
					$data['Logo']->move($app['management.base_path'], $nextAvailableNumberInBasePath . '.jpg');
                    $data['Logo'] = $nextAvailableNumberInBasePath . '.jpg';

				} else {
					$companyform->get('Logo')->addError(new \Symfony\Component\Form\FormError('Only .jpg allowed'));
				}

                // write to database if everything is ok
                if ($isValid) {
                    $app['users']->updateCompany($data, $user, $app);
                    // update session with new info
                    $sessionId = $app['session']->get('user')['id'];
                    $app['session']->set('user', array(
                        'id' => $sessionId,
                        'username' => $data['Email'],
                        'company' => $data['Name']
                    ));
                    // go back to the overview
                    return $app->redirect($app['url_generator']->generate('management.overview'));
                }
            }
        }
        // when there is no custom logo -> replace by default
        if ($user['logo'] === null) {
            $user['logo'] = $app['management.base_url'] . '/default-logo.jpg';
        } else {
            $user['logo'] =  $app['management.base_url'] . '/' . $user['logo'];
        }

        return $app['twig']->render('management/company.twig', array('companyform' => $companyform->createView(), 'logo' => $user['logo'], 'user' => $app['session']->get('user')));
    }


    /**
     * Check if the user is logged in
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function checkLogin(Request $request, Application $app) {
        if (!$app['session']->get('user')) {
            return $app->redirect($app['url_generator']->generate('auth.login'));
        }
    }
}