<?php

// Bootstrap
require __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

// Use Request from Symfony Namespace
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->error(function (\Exception $e, $code) use ($app) {
    if ($code == 404) {
        return $app['twig']->render('errors/404.twig', array('error' => $e->getMessage()));
    } else {
        return 'Shenanigans! Something went horribly wrong // ' . $e->getMessage();
    }
});

// Enable twig text extension to truncate strings
//@see https://bacardi55.org/2013/02/02/add-an-twig-extension-in-silex.html#/
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    $twig->addExtension(new Twig_Extensions_Extension_Text($app));
    return $twig;
}));


$app->get('/auth', function (Silex\Application $app) {
    return $app->redirect($app['request']->getBaseUrl() . '/auth');
})->bind('home');

// Define routes for our static pages
$pages = array(
	'/' => 'home',
    '/about' => 'about'
);
foreach ($pages as $route => $view) {
	$app->get($route, function () use ($app, $view) {
		return $app['twig']->render('static/' . $view . '.twig');
	})->bind($view);
}

// Mount our controllers (dynamic routes)
$app->mount('/internships', new Samvandenberge\Provider\Controller\FrontController());
$app->mount('/auth', new Samvandenberge\Provider\Controller\AuthController());
$app->mount('/management', new Samvandenberge\Provider\Controller\ManagementController());