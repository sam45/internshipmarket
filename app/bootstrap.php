<?php

// Require Composer Autoloader
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

// Create new Silex App
$app = new Silex\Application();

// App Configuration
$app['debug'] = false;

// Use FormServiceProvider - built in service provider
$app->register(new Silex\Provider\FormServiceProvider());

// Use TranslationServiceProvider (with empty message)
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

// Use Validator Service Provider
$app->register(new Silex\Provider\ValidatorServiceProvider());

// Use Form Service Provider
$app->register(new Silex\Provider\FormServiceProvider());

// Use UrlGenerator - built in service provider
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Use Session Service Provider
$app->register(new Silex\Provider\SessionServiceProvider());

// Use Twig - template engine
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'views')
);

// Use ConfigServiceProvider
$app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__ . '/config.php'));

// Use Doctrine
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
        'db.options' => $app['db.options']
    )
);

// Use docterine
$app->register(new Silex\Provider\SwiftmailerServiceProvider(), array(
     'swiftmailer.options' => $app['mail']
    )
);

// Use pagerfanta
$app->register(new FranMoreno\Silex\Provider\PagerfantaServiceProvider(), array(
    $app['pagerfanta.view.options'] = array(
    'routeName'     => null,
    'routeParams'   => array(),
    'pageParameter' => '[page]',
    'proximity'     => 3,
    'next_message'  => '&raquo;',
    'prev_message'  => '&laquo;',
    'default_view'  => 'default'
    )
));

// Use Repository Service Provider
$app->register(new Knp\Provider\RepositoryServiceProvider(), array(
    'repository.repositories' => array(
        'users' => 'Samvandenberge\\Repository\\CompaniesRepository',
        'internships' => 'Samvandenberge\\Repository\\InternshipsRepository',
        'categories' => 'Samvandenberge\\Repository\\CategoriesRepository',
        'subcategories' => 'Samvandenberge\\Repository\\SubcategoriesRepository'
    )
));