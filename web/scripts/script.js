// Enable the new visual refresh for google maps
google.maps.visualRefresh = true;

$(function () {

    /*------------------------------------------------------------------------
    * Deletion confirmation dialog
    * ------------------------------------------------------------------------
    */

    $('.onDelete').on('click', function (e) {
        if (confirm('Are you sure you want to delete this internship?')) {
            // Save it!
        } else {
            e.preventDefault();
        }
    });


    /*------------------------------------------------------------------------
    * Date picker
    * ------------------------------------------------------------------------
    */

    $('.date').datepicker ({ dateFormat: 'dd-M-yy' })


    /*------------------------------------------------------------------------
    * Google Maps init
    * ------------------------------------------------------------------------
    */
    if ($('#map-canvas').length > 0) {
        google.maps.event.addDomListener(window, 'load', initialize);
    }


    /*------------------------------------------------------------------------
    * Upload loader
    * ------------------------------------------------------------------------
    */

    // hide on page load
    $('.loader').hide();
    // show on form submit
    $('#formUpload').on('click', function(e) {
        $('.loader').show();
    });


    /*------------------------------------------------------------------------
    * Homescreen background
    * ------------------------------------------------------------------------
    */

    // indien de pagina de homepage is -> andere backgroundwallpaper inladen
    if ($('#homePage').length > 0) {
        $('#wrapper').addClass('clouds');
    } else {
        $('#wrapper').removeClass('clouds');
    }


    /*------------------------------------------------------------------------
    * Photo carousel
    * ------------------------------------------------------------------------
    */

    $('.carousel').carousel({
        interval: 5000
    })


});

    /*------------------------------------------------------------------------
    * Google Maps
    * ------------------------------------------------------------------------
    */

    function initialize() {
        var lat = document.getElementById('map-canvas').getAttribute('data-lat');
        var long = document.getElementById('map-canvas').getAttribute('data-long');

        console.log(lat);
        console.log(long);

        var mapOptions = {
            center: new google.maps.LatLng(lat, long),
            zoomControl: true,
            panControl: true,
            streetViewControl: true,
            zoom: 12,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, long),
            map: map,
            title: "Hello World!"
        });
    }