$(function () {

    /*------------------------------------------------------------------------
    * Deletion confirmation dialog
    * ------------------------------------------------------------------------
    */

    $('.onDelete').on('click', function (e) {
        if (confirm('Are you sure you want to delete this internship?')) {
            // Save it!
        } else {
            e.preventDefault();
        }
    });


    /*------------------------------------------------------------------------
    * Date picker
    * ------------------------------------------------------------------------
    */

    $('.date').datepicker ({ dateFormat: 'dd-mm-yy' })



    });