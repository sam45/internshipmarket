Website voor serverside webscripten (PHP)

Keywords: PHP, Silex, MSSQL

/**
* Companies
* ------------------------------
*/

	info@cde.int
	Azerty123

	info@trepartment.com
	Azerty123

	clear@hotmail.com
	Clear

	sam_vdb4@hotmail.com
	Azerty123



/**
* Notes
* ------------------------------
*/

	- File upload doesn't work on the webspace because I can't modify the permissions from home.
	- Monolog service provider doesn't work due to no write permissions on the log file.



/**
* Sources
* ------------------------------
*/

	http://www.globalplacement.com/
	https://github.com/bramus/ws2-sws-course-materials
	http://symfony.com/
	http://silex.sensiolabs.org/
	http://glyphicons.com