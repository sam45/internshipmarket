-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 23, 2013 at 10:10 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `internshipmarket`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `countries_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_addresses_countries1_idx` (`countries_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `address`, `city`, `postal_code`, `countries_id`) VALUES
(1, '2 avenue Edmond Van Nieuwenhuyse', 'Brussels', '1160', 2),
(2, 'Österjörn 13', 'jörn', '93055', 26),
(3, 'Steenweg 68', 'Zwalm', '9630', 2),
(4, 'Ginnekenmarkt 15', 'Breda', NULL, 19),
(5, 'Konrad-Adenauer-Ring 25', 'Freigericht', '63579', 10);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Arts, Visual and Performing'),
(2, 'Sports and Fitness'),
(3, 'Agriculture'),
(4, 'Business and Finance'),
(5, 'Management'),
(6, 'Office and Administrative Support'),
(7, 'Sales'),
(8, 'Health Care Support'),
(9, 'Health Diagnosis and Treatment'),
(10, 'Health Technologie'),
(11, 'Media and Communications'),
(12, 'Social Science'),
(13, 'Community and Social Services'),
(14, 'Education, Museum Work and Library Science'),
(15, 'Law and Government'),
(16, 'Protective Services'),
(17, 'Architecture, Engineering and Drafting'),
(18, 'Computers and Math'),
(19, 'Environment'),
(20, 'Science'),
(21, 'Construction'),
(22, 'Installation and Repair'),
(23, 'Personal Care and Culinary Services'),
(24, 'Production');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addresses_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `website` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_companies_addresses_idx` (`addresses_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `addresses_id`, `name`, `password`, `logo`, `tel`, `fax`, `email`, `website`) VALUES
(1, 1, 'cde', 'msx.pkl3XXYro', '4.jpg', '+3226791811', '+3226752603', 'info@cde.int', 'http://www.cde.int/'),
(2, 2, 'trepartment', 'msx.pkl3XXYro', NULL, NULL, NULL, 'info@trepartment.com', 'http://www.trepartment.com'),
(3, 3, 'Clear', 'msR6g4QL5FlFo', NULL, NULL, NULL, 'Clear@hotmail.com', NULL),
(4, 4, 'Agri', 'msx.pkl3XXYro', '5.jpg', NULL, NULL, 'sam_vdb4@hotmail.com', NULL),
(5, 5, 'Test', 'msx.pkl3XXYro', NULL, NULL, NULL, 'test@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`) VALUES
(1, 'Austria'),
(2, 'Belgium'),
(3, 'Bulgaria'),
(4, 'Cyprus'),
(5, 'Czech Republic'),
(6, 'Denmark'),
(7, 'Estonia'),
(8, 'Finland'),
(9, 'France'),
(10, 'Germany'),
(11, 'Greece'),
(12, 'Hungary'),
(13, 'Ireland'),
(14, 'Italy'),
(15, 'Latvia'),
(16, 'Lithuanaia'),
(17, 'Luxembourg'),
(18, 'Malta'),
(19, 'Netherlands'),
(20, 'Poland'),
(21, 'Portugal'),
(22, 'Romania'),
(23, 'Slovakia'),
(24, 'Slovenia'),
(25, 'Spain'),
(26, 'Sweden'),
(27, 'United Kingdom');

-- --------------------------------------------------------

--
-- Table structure for table `internships`
--

DROP TABLE IF EXISTS `internships`;
CREATE TABLE IF NOT EXISTS `internships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companies_id` int(11) NOT NULL,
  `subcategories_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `free_places` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_internships_companies1_idx` (`companies_id`),
  KEY `fk_internships_subcategories1_idx` (`subcategories_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `internships`
--

INSERT INTO `internships` (`id`, `companies_id`, `subcategories_id`, `name`, `free_places`, `description`, `date_start`, `date_end`) VALUES
(1, 1, 30, 'Web Developer / Webmaster', 1, '<p>The CDE is an ACP (African, Caribbean and Pacific)/EU joint Institution created in the framework of the Cotonou Agreement. CDE''s financial resources mainly come from the European Development Fund (EDF). Its objective is to ensure the development of professional ACP enterprises operating in the private sector. CDE operates in complementarity with the European Commission, the Secretariat of the ACP Group of States and the European Investment Bank (EIB) in the framework of support to the private sector.</p>\r\n<p><strong>The CDE offers internships at its headquarters in Brussels and is currently looking for a trainee to join its IT team with the following profile: Web developer / webmaster The trainee would help the Centre in revamping its website.</strong></p>\r\n<p>The internship is a good opportunity to acquire experience in the field of web developing / web mastering. The Centre offers contracts of a probationary period of three months, renewable several times for a maximum of one year. The trainee must be a citizen from an EU or ACP country, he/she must be a student in his final year or recently graduated in Computing Science or equivalent. He/she should have good written level of English and French. Experience in creating and developing websites, blogs, web 2.0 would be an advantage. He/she must also be dynamic, proactive and able to work in a multicultural environment. The age requirement is 30 years maximum. The internship will start as soon as possible.The allowance paid to the trainee shall be &nbsp;750 flat per month. The Centre shall be responsible for the medical coverage (health, accident) during the period of the trainee''s contract. All other obligations regarding social security or taxation shall remain the obligation of the trainee.Should you be interested, please send your CV and a cover letter together with a copy of your University Diploma or a Certificate of studies to the following address: jobs@cde.int. Please visit www.cde.int to become familiar with the Institution.</p>', '2013-02-10', '2013-06-10'),
(2, 1, 30, 'Software Developer  / Tester Internship', 1, '<p>We offer a six month job experience position in Brussels in 2013.</p>\r\n<p><strong>We are looking for a software developer/tester that can help us to test our applications and that slowly can start helping us developing new applications.</strong> We use C++, PHP, HTML and MySQL mainly. Please provide source code so we can evaluate your style.</p>\r\n<p>We are looking for interns that have finished their studies and that might have the possibility of keep working for us once the internship is over.</p>\r\n<p>A normal working day is 8 hours from 9 am to 6 pm. We can offer you a work station including a computer and an IP telephone.</p>\r\n<p>We will invite the students to an interview where more detailed questions can be answered.</p>', '2013-07-18', '2014-02-13'),
(3, 1, 30, 'Mobile App Developers', 8, '<p>Based in the thriving city of Brusseles, Belgium, a smartphone software firm is hiring a team to develop a new series of search based mobile apps. Daily DutiesWorking with the development team to:Perform technical analyses of requirements Produce solid, detailed technical designs Develop the statistical algorithms behind pattern recognition and recommendation features Write clean, modular, robust code to implement desired requirements &nbsp;</p>\r\n<p><strong>Work with QA and Customer Support to triage and fix bugs with rapid turnaround Contribute ideas for making the applications better and easier to use Candidate Requirements Sufficient level of English is recommended.</strong></p>\r\n<p>Experience developing Java and/or Objective-C code Familiarity with APIs for platforms like: Facebook, Twitter, and Google Maps Familiarity with database development tools like SQL We are seeking rising juniors with strong IT experience Applicants should demonstrate a standard academic record (minimum Baccalaureate) Adequate writing skills and attention to details Ability to work both independently and on a team</p>', '2014-01-01', '2014-06-15'),
(4, 2, 47, 'Translator French - English', 1, '<p><strong>Independent, responsible and organized person to translate from French to English</strong></p>\r\n<p>Excellent mastery of the English language</p>\r\n<p>Very good knowledge of the French language</p>\r\n<p>Fluent written expression in both languages</p>\r\n<p>Good general knowledge and interest in languages and travel</p>\r\n<p>&nbsp;</p>\r\n<p>We offer an interesting and varied training program at the heart of a young, dynamic team.<br />A salary is paid monthly and accommodation is available if required.<br /><br /></p>\r\n<ul>\r\n<li><span style="font-size: 11px;">Student Requirements</span></li>\r\n<li><span style="font-size: 11px;">Education Level : A-Level/Higher</span></li>\r\n<li><span style="font-size: 11px;">Field Of Expertise : Linguistics</span></li>\r\n<li><span style="font-size: 11px;">Languages : French, English</span></li>\r\n<li><span style="font-size: 11px;">Motivation Is Required : No</span></li>\r\n<li><span style="font-size: 11px;">CV Is Required : No</span></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', '2014-05-23', '2014-08-28'),
(5, 2, 16, 'Sales people', 4, '<p>We are looking for killer sales people to join our expert Sales team. Founded in 2011, our company is a dynamic software start-up company based in Berlin. We offer complete Feedback Solutions for all kinds of online and offline businesses. We collect and display customers'' opinions on our widgets, helping online sellers earn customers'' trust and making the eCommerce market a safer place.</p>\r\n<p>&nbsp;</p>\r\n<p>Your Profile:</p>\r\n<p>-Native English, Italian, German and or French speaker, German is a plus.</p>\r\n<p>-Previous experience in sales.</p>\r\n<p>- Exceptional interpersonal skills, customer-focused, strong communication and organizational skills, goal oriented, and self-motivated team player</p>\r\n<p>- Strong negotiation skills and multicultural adaptability.</p>\r\n<p>- You have a real interest in Internet Technologies and e-Commerce.</p>\r\n<p>&nbsp;</p>\r\n<p>Your responsibilities:</p>\r\n<p>- Acquisition of new customers.</p>\r\n<p>-Asking questions, listening to answers, positioning our product competitively so you can close business</p>\r\n<p>-Negotiate pricing and contract terms and conditions.</p>\r\n<p>- Effectively coordinate all aspects of the sale from start to finish.</p>\r\n<p>&nbsp;</p>\r\n<p>We offer :</p>\r\n<p>&nbsp;</p>\r\n<p>- Daily challenging projects.</p>\r\n<p>- A relaxed working environment with flexible working time and a flat hierarchy.</p>\r\n<p>- A young and multicultural team.</p>\r\n<p>- Breakfast and fresh fruit every day.&nbsp;</p>', '2014-09-12', '2014-12-31'),
(6, 2, 47, 'Communication', 2, '<p>Responsibilities:- <strong>Communication with suppliers, customers and partners via direct interaction</strong>, mail and phone- Conducting analyses; marketing or financially related- Creation of reports, e.g. forecasts- Supporting the sales processes by creating and handling of orders- Event and travel planning- Data management and maintenance of CRM systems- General administration tasks, e.g. filing expense reports- Holding of presentation if needed Skills required:- Very Good MS Office skills- Very good communication skills of English, Spanish helpful- Can work independently as well as in a team- Detail oriented and flexible- Good planning and multitasking skills What you can expect:- First-hand work experience of how a software business functions- Broad experiences in various business areas- An empowering and pleasant working atmosphere- Internship allowance (to be discussed)</p>', '2014-05-01', '2014-11-01'),
(7, 2, 49, 'Office Administrator', 1, '<p>Providing employees with all the necessary information for their successful performance. Managing the office inventory and chancellery. Participating in different organizational issues and events. Assuring the company&rsquo;s smooth IT operations and liasing with outside IT consultants. Keeping track of the company&rsquo;s IT status and reporting about its progress. Providing employees with IT support when needed and eliminating IT-related issues. Setting up user accounts. Reviewing the company&rsquo;s invoices and payments related to HR expenses. Developing the budgets for different organizational matters, including HR. Keeping track and dealing with company&rsquo;s travel. Assisting HR and Accounting departments with reviewing the budgets. In Order to Be considered with Priority, Pls send your CV Competencies &amp; Knowledge Proficient English language skills, written and verbal. Less than ordinary IT knowledge and skills, with flexibility to learn. Detail orientation. Discipline and ability to work independently with minimum supervision and to work with the team. Excellent in time management, self organization and stress management. Excellent interpersonal skills. Excellent communication skills, including telephone etiquette. <strong>Multitasking ability. Flexibility. Pro-activity</strong>. Requirements Academic education in a business-related field: International Business and Management Studies, IT, Business and Economics, International Management, etc. (Bachelor &amp; Master Degrees)&nbsp;</p>\r\n<p>Student Requirements</p>\r\n<p>Education Level : 300 Level/BSc./B. Tech/B. Eng</p>\r\n<p>Field Of Expertise : Administration</p>\r\n<p>Languages : English</p>\r\n<p>Motivation Is Required : Yes</p>\r\n<p>CV Is Required : Yes</p>', '2014-06-19', '2014-10-19'),
(8, 2, 8, 'Trainee Marketing Department Functional Perfu', 2, '<p>For ambitious, creative students we offer challenging internships.We are looking for :Two Trainees in our Marketing Department Functional Perfumeryv Work on customer presentations, market analysis and marketing reportsv Gather new product information and organise new product sessionsv Analyse consumer needs v Focus on brand positioning, benefits and ingredientsv Take care of New product purchase and requests :The student will be guided and tutored by the our Regional Category Marketing Director. The goal of the internship is to acquire marketing expertise in the B2B market and assist the marketing team in the successful execution of the business strategies within the region.We are looking for BSBA or MBA students in Marketing or Perfumery, fluent in English and another language.&nbsp;</p>\r\n<ul>\r\n<li><span style="font-size: 11px;">Student Requirements</span></li>\r\n<li><span style="font-size: 11px;">Education Level : Master''s Degree</span></li>\r\n<li><span style="font-size: 11px;">Field Of Expertise : Administration</span></li>\r\n<li><span style="font-size: 11px;">Languages : English</span></li>\r\n<li><span style="font-size: 11px;">Motivation Is Required : Yes</span></li>\r\n<li><span style="font-size: 11px;">CV Is Required : Yes</span></li>\r\n</ul>', '2014-05-16', '2014-11-30'),
(9, 2, 12, 'HR Intern', 1, '<p>ResponsibilitiesAs a HR Generalist Intern, you will support a number of HR Departments:Staffing: Support candidate sourcing activities including database searching to find relevant CV''s to match job requisitions. Support of day to day recruitment activities from advertising to interviewing. Compensation &amp; Benefits: Support in NL C&amp;B policies, projects &amp; ad-hoc queries. Engagement with local stakeholders on C&amp;B communications and updates as required.HR Services: Support as required to include Ad-hoc projects, complex query investigation/resolution; audit information requests and communications requirements.Support to HR Manager as required: Drafting and updating HR policies and procedures. Providing on support on various projects such as external benchmarking surveys. &nbsp;</p>', '2014-07-16', '2014-09-10'),
(10, 3, 30, 'Mobile App Developer', 6, '<p>Based in the thriving city of Brusseles, Belgium, a smartphone software firm is hiring a team to develop a new series of search based mobile apps. Daily DutiesWorking with the development team to:Perform technical analyses of requirements &middot; Produce solid, detailed technical designs&middot; Develop the statistical algorithms behind pattern recognition and recommendation features&middot; Write clean, modular, robust code to implement desired requirements &middot; Work with QA and Customer Support to triage and fix bugs with rapid turnaround&middot; Contribute ideas for making the applications better and easier to useCandidate Requirements&middot; Sufficient level of English is recommended&middot; Experience developing Java and/or Objective-C code&middot; Familiarity with APIs for platforms like: Facebook, Twitter, and Google Maps&middot; Familiarity with database development tools like SQL&middot; We are seeking rising juniors with strong IT experience&middot; Applicants should demonstrate a standard academic record (minimum Baccalaureate)&middot; Adequate writing skills and attention to details&middot; Ability to work both independently and on a team&nbsp;</p>', '2014-05-22', '2014-10-10'),
(11, 3, 16, 'Sales and Marketing internship', 1, '<h2 style="margin: 0px 0px 1.12em; padding: 0px; border: 0px; outline: 0px; color: #666666; font-size: 1.28em; line-height: 1.12em; font-family: Arial;">Job Description</h2>\r\n<p style="margin: 0px 0px 1.54em; padding: 0px; border: 0px; outline: 0px; font-size: 17px; line-height: 1.54em; color: #666666; font-family: Arial;">Clear is a fast-growing start-up that develops innovative technical solutions in the area of partnership, sponsorship, patronage and corporate patronage. It is backed by the Brussels Region, ING Bank and Business Angels actively involved in its evolution.</p>\r\n<p style="margin: 0px 0px 1.54em; padding: 0px; border: 0px; outline: 0px; font-size: 17px; line-height: 1.54em; color: #666666; font-family: Arial;">Our customers are BNP Paribas, Volkswagen, Carlsberg, Randstad, Audi, Delhaize, RTL, Carrefour, Bacardi, D&eacute;cathlon, L&rsquo;Or&eacute;al, Fondation SFR, Fondation EDF, BMW, Volvo, Fondation Air France, Fondation Groupama, Ice-Watch, etc.</p>\r\n<p style="margin: 0px 0px 1.54em; padding: 0px; border: 0px; outline: 0px; font-size: 17px; line-height: 1.54em; color: #666666; font-family: Arial;"><strong style="margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit;">Missions:</strong></p>\r\n<p style="margin: 0px 0px 1.54em; padding: 0px; border: 0px; outline: 0px; font-size: 17px; line-height: 1.54em; color: #666666; font-family: Arial;">&bull; Market surveys<br />&bull; Sales operational support (prospection, database management, etc.) in collaboration with our business developer in charge of the United Kingdom.<br />&bull; Marketing operational support: preparation of sales support tools, online communication (social media, blog, e-mailings, etc.) and internal marketing updates (customer support, FAQ, etc.).</p>\r\n<p style="margin: 0px 0px 1.54em; padding: 0px; border: 0px; outline: 0px; font-size: 17px; line-height: 1.54em; color: #666666; font-family: Arial;"><strong style="margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit;">Profile:</strong></p>\r\n<p style="margin: 0px 0px 1.54em; padding: 0px; border: 0px; outline: 0px; font-size: 17px; line-height: 1.54em; color: #666666; font-family: Arial;">&bull; Student in Business, Economics, Sales &amp; Marketing / Communication<br />&bull; English is mandatory<br />&bull; Autonomous and teamwork minded<br />&bull; Results driven<br />&bull; Start-up minded<br />&bull; A previous internship in the same field is a plus</p>\r\n<p style="margin: 0px 0px 1.54em; padding: 0px; border: 0px; outline: 0px; font-size: 17px; line-height: 1.54em; color: #666666; font-family: Arial;"><strong style="margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit;">We offer:</strong></p>\r\n<p style="margin: 0px 0px 1.54em; padding: 0px; border: 0px; outline: 0px; font-size: 17px; line-height: 1.54em; color: #666666; font-family: Arial;">&bull; A young, dynamic and international-minded working atmosphere<br />&bull; An experienced management team<br />&bull; The opportunity to be part of a fast-growing project and to evolve within the company</p>', '2014-04-02', '2014-06-27'),
(12, 4, 9, 'Field Scout Intern', 8, '<p style="font-family: verdana, ''ms sans serif'', arial;"><strong>Primary Role of Position:</strong></p>\r\n<p style="font-family: verdana, ''ms sans serif'', arial;">The Field Scout works closely with field sales and the operations support team.&nbsp; Responsibilities may include pest trap placement, monitoring/recording pest activity of various crops and pests, soil sampling, tissue sampling, calibration of yield monitors, Veris and boundary field mapping.&nbsp; Training will be provided for specific tasks.</p>\r\n<p style="font-family: verdana, ''ms sans serif'', arial;"><strong>Specific Responsibilities Include</strong>:</p>\r\n<ul style="font-family: verdana, ''ms sans serif'', arial;">\r\n<li>Prepare traps for placement in the field.</li>\r\n<li>Travel to fields as directed and install traps in trees/fields.</li>\r\n<li>Inspect traps on a weekly basis.</li>\r\n<li>Empty traps and reset for next week.</li>\r\n<li>Enter and maintain data counts into Excel spreadsheet.</li>\r\n<li>Relay information to the appropriate sales representative and support staff.</li>\r\n<li>Take soil and tissue samples as directed.</li>\r\n<li>Scout crops for economic pests.</li>\r\n<li>Map field boundaries.</li>\r\n<li>Perform zone and grid sampling.</li>\r\n<li>Calibration of yield monitors.</li>\r\n<li>Comply with company safety standards.</li>\r\n</ul>\r\n<p style="font-family: verdana, ''ms sans serif'', arial;"><strong>Skills and Experience Required:</strong></p>\r\n<ul style="font-family: verdana, ''ms sans serif'', arial;">\r\n<li>Education:\r\n<ul>\r\n<li>Required: High School Diploma or equivalent.</li>\r\n<li>Preferred:&nbsp; Currently working towards a Bachelor&rsquo;s Degree in Agriculture.</li>\r\n</ul>\r\n</li>\r\n<li>Good math skills and the ability to maintain accurate counts.</li>\r\n<li>Requires a valid driver license.</li>\r\n<li>Dependable and able to work without direct supervision.</li>\r\n<li>Physically able to endure outdoor climates including hot or cold weather extremes.</li>\r\n<li>Good interpersonal skills.</li>\r\n<li>Proficiency with standard software including MS Office Suite and basic GIS tools.</li>\r\n<li>Familiar with and willing to operate farm equipment.</li>\r\n<li>Ability to operate an all terrain vehicle (ATV)/4-wheeler).</li>\r\n</ul>', '2014-05-01', '2014-10-01'),
(13, 4, 8, 'Aquaculturists needed', 2, '<p style="margin: 10px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; list-style: none; color: #6a3000; line-height: 22px;">We are a community-based educational and production farm on an 11-acre property owned by the Santa Clara Unified School District. We operate a diverse urban agriculture operation that includes an on-site Farm Stand, a CSA Program, and an Educational Garden; in addition we conduct workshops, tours, field trips and Volunteer Events. We use sustainable agriculture methods and follow national organic standards. We have a small staff and rely primarily on community of thousands of volunteers, student interns, and corporate groups.</p>\r\n<p style="margin: 10px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; list-style: none; color: #6a3000; line-height: 22px;">As an intern at Full Circle Farm you are a valued member of the organization responsible for daily management of our production farm, education garden, nursery, farm stand, and volunteers. The most important requirements are enthusiasm, willingness to learn, and dedication. The primary focus of our internship is to give you a well-rounded education as to how to run a farm.</p>\r\n<p style="margin: 10px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; list-style: none; color: #6a3000; line-height: 22px;">Every few weeks, we will have you focus primarily on one aspect of the farm. Those aspects are listed below. In addition to hands-on, experiential learning, we off readings followed by discussion and some class time on various topics.&nbsp; Interns are also encouraged to design and implement an individual project.</p>\r\n<p style="margin: 10px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; list-style: none; color: #6a3000; line-height: 22px;">We welcome anyone with a career interest in farming, gardening, non-profit organizations, or sustainable food systems. Previous experience in these fields is preferred but not required&ndash;you will learn all you need to know in order to get the job done.</p>', '2014-09-01', '2015-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `categories_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_subcategorie_categories1_idx` (`categories_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `name`, `categories_id`) VALUES
(1, 'Actors', 1),
(2, 'Art Directors', 1),
(3, 'Web Designers', 1),
(4, 'Photgraphers', 1),
(5, 'Illustrators', 1),
(6, 'Athletes', 2),
(7, 'Coaches and Scouts', 2),
(8, 'Aquaculturists', 3),
(9, 'Crop Farmers', 3),
(10, 'Budget Analysts', 4),
(11, 'Property Managers', 5),
(12, 'Human Resources Managers', 5),
(13, 'Administrative Assistants and Secretaries ', 6),
(14, 'Human Resources Assistants', 6),
(15, 'Travel Agents', 7),
(16, 'Sales Engineers', 7),
(17, 'Dental Assistants', 8),
(18, 'Medical Assistants', 8),
(19, 'Radiologists', 9),
(20, 'Surgeons', 9),
(21, 'Nuclear Medicine Technologists', 10),
(22, 'Cardiovascular Technologists', 10),
(23, 'Priests', 13),
(24, 'Archivists', 14),
(25, 'Librarians', 14),
(26, 'Judges', 15),
(27, 'Detectives', 16),
(28, 'Electrical Engineers', 15),
(29, 'Mechanical Engineers', 15),
(30, 'Computer Programmers', 18),
(31, 'Software Developers', 18),
(32, 'Network System and Data Communcations Analyst', 18),
(33, 'Database Administrators', 18),
(34, 'Writers', 11),
(35, 'Camera Operators and Editors', 11),
(36, 'Historians', 12),
(37, 'Architects', 17),
(38, 'Park Ranger', 19),
(39, 'Science Technicians', 20),
(40, 'Medical Scientists', 20),
(41, 'Roofers', 21),
(42, 'Computer-Repair Technicians', 22),
(43, 'Personal and Home Care Aides', 23),
(44, 'Welders', 24),
(45, 'Web Developer', 18),
(46, 'Mobile App Developer', 18),
(47, 'Tolk', 11),
(48, 'News Analysts, Reporters, and Correspondents', 11),
(49, 'Office and Administrative Support', 4),
(50, 'Advertising, Marketing, and Public Relations ', 5),
(51, 'Human Resources Managers', 5);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `fk_addresses_countries1` FOREIGN KEY (`countries_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `fk_companies_addresses` FOREIGN KEY (`addresses_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `internships`
--
ALTER TABLE `internships`
  ADD CONSTRAINT `fk_internships_companies1` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_internships_subcategories1` FOREIGN KEY (`subcategories_id`) REFERENCES `subcategories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `fk_subcategorie_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
